package com.boxes.driver.data.repository

import com.boxes.driver.base.BaseRepository
import com.boxes.driver.data.remote.BoxesAPI
import com.boxes.driver.data.request.MarkDelivered

class OrderDetailsRepository(private val api: BoxesAPI): BaseRepository() {

    suspend fun getOrder(id: String) = handleResponse { api.getOrderDetails(id) }

    suspend fun markDelivered(request: MarkDelivered) = handleResponse { api.markDelivered(request) }

}