package com.boxes.driver.data.repository

import com.boxes.driver.base.BaseRepository
import com.boxes.driver.data.remote.BoxesAPI
import com.boxes.driver.data.request.ChangePassword

class ChangePasswordRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun changePassword(request: ChangePassword) = handleResponse { api.changePassword(request) }

}