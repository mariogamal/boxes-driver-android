package com.boxes.driver.data.request

data class DateFilter(val date: String? = null)
