package com.boxes.driver.data.local

import android.content.Context
import android.content.SharedPreferences
import com.boxes.driver.data.model.User
import com.google.gson.Gson

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class Preferences(context: Context) {

    private val preferences: SharedPreferences =
            context.getSharedPreferences("boxes-pref", Context.MODE_PRIVATE)

    fun saveUserToken(value: String) {
        preferences.edit().putString(TOKEN, value).apply()
    }

    fun getUserToken(): String? {
        return preferences.getString(TOKEN, null)
    }

    fun saveUserModel(value: User) {
        val user = Gson().toJson(value)
        preferences.edit().putString(USER, user).apply()
    }

    fun getUserModel(): User? {
        val user = preferences.getString(USER, null)
        return Gson().fromJson(user, User::class.java)
    }

    fun saveLanguageId(id: String) {
        preferences.edit().putString(LANGUAGE, id).apply()
    }

    fun getLanguageId(): String? {
        return preferences.getString(LANGUAGE, null)
    }

    fun clearData() {
        preferences.edit().clear().apply()
    }

    companion object {
        const val USER = "user"
        const val TOKEN = "token"
        const val LANGUAGE = "language"
    }
}