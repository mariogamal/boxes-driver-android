package com.boxes.driver.data.request

import com.google.gson.annotations.SerializedName

data class MarkDelivered(@SerializedName("order_id") val id: String)
