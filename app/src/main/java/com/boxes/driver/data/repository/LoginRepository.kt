package com.boxes.driver.data.repository

import com.boxes.driver.base.BaseRepository
import com.boxes.driver.data.remote.BoxesAPI
import com.boxes.driver.data.request.LoginRequest

class LoginRepository(private val api: BoxesAPI): BaseRepository() {

    suspend fun login(request: LoginRequest) = handleResponse { api.login(request) }

}