package com.boxes.driver.data.model

import com.google.gson.annotations.SerializedName

data class Order(
    @SerializedName("order_id")
    val orderID: String,
    val status: String,
    val name: String,
    val phone: String,
    val floor: String,
    val block: String,
    val apartment: String,
    val building: String,
    val street: String,
    val city: String,
    val area: String,
    val meals: List<Meal>
)
