package com.boxes.driver.data.repository

import com.boxes.driver.base.BaseRepository
import com.boxes.driver.data.remote.BoxesAPI
import com.boxes.driver.data.request.DateFilter

class OrderListRepository(private val api: BoxesAPI): BaseRepository() {

    suspend fun getOrders() = handleResponse { api.getOrders() }

    suspend fun getHistory(date: DateFilter) = handleResponse { api.getHistory(date) }

}