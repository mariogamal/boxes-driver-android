package com.boxes.driver.data.remote

import com.boxes.driver.data.model.Login
import com.boxes.driver.data.model.Order
import com.boxes.driver.data.model.Response
import com.boxes.driver.data.request.ChangePassword
import com.boxes.driver.data.request.DateFilter
import com.boxes.driver.data.request.LoginRequest
import com.boxes.driver.data.request.MarkDelivered
import retrofit2.http.*

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
interface BoxesAPI {

    @POST("v1/driver/login")
    suspend fun login(@Body request: LoginRequest): Response<Login>

    @GET("v1/driver/auth/list-orders")
    suspend fun getOrders(): Response<List<Order>>

    @POST("v1/driver/auth/history-order")
    suspend fun getHistory(@Body date: DateFilter): Response<List<Order>>

    @GET("v1/driver/auth/order-details")
    suspend fun getOrderDetails(@Query("order_id") id: String): Response<Order>

    @POST("v1/driver/auth/change-order-status")
    suspend fun markDelivered(@Body request: MarkDelivered): Response<Any>

    @POST("v1/driver/auth/change-password")
    suspend fun changePassword(@Body request: ChangePassword): Response<Any>
}
