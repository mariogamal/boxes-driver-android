package com.boxes.driver.data.model

data class Product(
    val name: String,
    val caption: String,
    val image: String
)
