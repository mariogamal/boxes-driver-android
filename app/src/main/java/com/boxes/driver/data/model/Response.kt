package com.boxes.driver.data.model

data class Response<T>(
       val status: Int,
       val message: String,
       val data: T
)