package com.boxes.driver.data.model

import com.google.gson.annotations.SerializedName

data class Login(
    @SerializedName("access_token")
    val token: String,
    @SerializedName("token_type")
    val type: String,
    val user: User
)
