package com.boxes.driver.data.model

import com.google.gson.annotations.SerializedName

data class Meal(@SerializedName("category_name") val categoryName: String, val product: Product)
