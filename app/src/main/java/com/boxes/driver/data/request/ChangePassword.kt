package com.boxes.driver.data.request

class ChangePassword(
    val current_password: String,
    val new_password: String
)