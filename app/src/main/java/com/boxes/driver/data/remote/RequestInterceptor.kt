package com.boxes.driver.data.remote

import com.boxes.driver.data.local.Preferences
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class RequestInterceptor(private val preferences: Preferences) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder = request.newBuilder()
        preferences.getLanguageId()?.let {
            builder.url(request.url
                    .newBuilder()
                    .addQueryParameter("language_id", it)
                    .build())
        }
        preferences.getUserToken()?.let { builder.header("Authorization", it) }
        return chain.proceed(builder.method(request.method, request.body).build())
    }

}