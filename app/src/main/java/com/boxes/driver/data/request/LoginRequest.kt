package com.boxes.driver.data.request

data class LoginRequest(
    val email: String,
    val password: String,
    val device_token: String? = "",
    val device_type: String = "android"
)
