package com.boxes.driver.ui.orderdetails

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boxes.driver.BuildConfig
import com.boxes.driver.R
import com.boxes.driver.data.model.Meal
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.meal_item_layout.view.*

class MealsAdapter(private val meals: List<Meal>) :
    RecyclerView.Adapter<MealsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.name
        val caption: TextView = view.caption
        val category: TextView = view.category
        val image: ImageView = view.image
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.meal_item_layout, viewGroup, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val meal = meals[position]
        holder.category.text = meal.categoryName
        holder.name.text = meal.product.name
        holder.caption.text = meal.product.caption
        Picasso.get().load(BuildConfig.IMG_URL + meal.product.image).into(holder.image)
    }

    override fun getItemCount() = meals.size

    companion object {
        const val ID = "id"
    }
}
