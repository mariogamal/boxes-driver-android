package com.boxes.driver.ui.login

import android.os.Bundle
import com.boxes.driver.R
import com.boxes.driver.base.BaseActivity
import com.boxes.driver.data.request.LoginRequest
import com.boxes.driver.ui.MainActivity
import com.boxes.driver.util.string
import com.onesignal.OneSignal
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        preferences.getUserModel()?.let {
            navigateToMain()
        }

        login.setOnClickListener {
            observe(viewModel.login(LoginRequest(email.string(), password.string(), OneSignal.getDeviceState()?.userId))) {
                preferences.saveUserToken(it.type + " " + it.token)
                preferences.saveUserModel(it.user)
                navigateToMain()
            }
        }
    }

    private fun navigateToMain() {
        start(MainActivity::class.java)
        finish()
    }
}