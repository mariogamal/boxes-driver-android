package com.boxes.driver.ui.orderlist

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.boxes.base.BaseFragment
import com.boxes.driver.R
import com.boxes.driver.ui.MainActivity
import com.boxes.driver.util.gone
import com.boxes.driver.util.visible
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_order_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class OrderListFragment : BaseFragment() {

    private val viewModel: OrderListViewModel by viewModel()
    private lateinit var filter: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_order_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        filter = (activity as MainActivity).filter
        filter.setOnClickListener {
            if (filter.tag == "0") showDatePickerDialog()
            else getHistory()
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 0) {
                    filter.gone()
                    viewModel.getOrders()
                } else {
                    filter.visible()
                    getHistory()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
        initialState()
        observe(viewModel.orders) { ordersRecycler.adapter = OrdersAdapter(it) }
        observe(viewModel.history) {
            ordersRecycler.adapter = OrdersAdapter(it)
            if (filter.tag == "1") {
                filter.text = "Clear"
            }
        }
    }

    private fun getHistory() {
        filter.tag = "0"
        filter.text = "Filter"
        viewModel.getHistory()
    }

    private fun initialState() {
        viewModel.getOrders()
        tabLayout.selectTab(tabLayout.getTabAt(0))
    }

    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(requireActivity(), { _, year, oldMonth, day ->
            val month = oldMonth + 1
            val date =
                "$year-" + (if (month > 9) month else "0$month") + "-" + if (day > 9) day else "0$day"
            viewModel.getHistory(date)
            filter.text = "Clear"
            filter.tag = "1"
        }, y, m, d)
        dpd.show()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getOrders()
    }
}