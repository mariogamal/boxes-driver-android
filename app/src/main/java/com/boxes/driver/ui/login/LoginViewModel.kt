package com.boxes.driver.ui.login

import androidx.lifecycle.liveData
import com.app.boxes.data.remote.Resource
import com.boxes.driver.base.BaseViewModel
import com.boxes.driver.data.repository.LoginRepository
import com.boxes.driver.data.request.LoginRequest

class LoginViewModel(private val repository: LoginRepository): BaseViewModel() {

    fun login(request: LoginRequest) = liveData {
        emit(Resource.Loading())
        emit(repository.login(request))
    }
}