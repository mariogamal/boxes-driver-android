package com.boxes.driver.ui.changepassword

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.boxes.driver.R
import com.boxes.driver.base.BaseActivity
import com.boxes.driver.data.request.ChangePassword
import com.boxes.driver.util.string
import kotlinx.android.synthetic.main.activity_change_password.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChangePasswordActivity : BaseActivity() {

    private val viewModel: ChangePasswordViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        back.setOnClickListener { finish() }
        changePassword.setOnClickListener {
            if (newPass.string() == confirmPass.string() && isValid(oldPass, newPass, confirmPass))
                observe(
                    viewModel.changePassword(
                        ChangePassword(
                            oldPass.string(),
                            newPass.string()
                        )
                    )
                ) {
                    finish()
                    Toast.makeText(this, "Password changed Successfully", Toast.LENGTH_SHORT).show()
                }
            else
                showMessage("Password should be at least 8 charecters")
        }
    }

    private fun isValid(vararg pass: EditText) = pass.filter { it.string().length > 7 }.size == pass.size
}