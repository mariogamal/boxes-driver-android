package com.boxes.driver.ui.orderlist

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boxes.driver.R
import com.boxes.driver.data.model.Order
import com.boxes.driver.ui.orderdetails.OrderDetailsActivity
import kotlinx.android.synthetic.main.order_item_layout.view.*

class OrdersAdapter(private val orders: List<Order>) :
    RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val area: TextView = view.area
        val address: TextView = view.address
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.order_item_layout, viewGroup, false)
        )

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val order = orders[position]
        val context = viewHolder.itemView.context
        viewHolder.area.text = order.city + ", " + order.area
        viewHolder.address.text = context.getString(R.string.address, order.block, order.street)
        viewHolder.itemView.setOnClickListener {
            if (order.status == "pending") {
                val intent = Intent(context, OrderDetailsActivity::class.java)
                intent.putExtra(ID, order.orderID)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount() = orders.size

    companion object {
        const val ID = "id"
    }
}
