package com.boxes.driver.ui.orderdetails

import androidx.lifecycle.liveData
import com.app.boxes.data.remote.Resource
import com.boxes.driver.base.BaseViewModel
import com.boxes.driver.data.repository.OrderDetailsRepository
import com.boxes.driver.data.request.MarkDelivered

class OrderDetailsViewModel(private val repository: OrderDetailsRepository): BaseViewModel() {

    fun orderDetails(id: String) = liveData {
        emit(Resource.Loading())
        emit(repository.getOrder(id))
    }

    fun markDelivered(id: String) = liveData {
        emit(Resource.Loading())
        emit(repository.markDelivered(MarkDelivered(id)))
    }

}