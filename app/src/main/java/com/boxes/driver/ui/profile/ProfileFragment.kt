package com.boxes.driver.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.boxes.base.BaseFragment
import com.boxes.driver.BuildConfig
import com.boxes.driver.R
import com.boxes.driver.ui.changepassword.ChangePasswordActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.fragment_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val user = preferences.getUserModel()!!
        val imageUrl = preferences.getUserModel()?.driver?.image
        firstName.setText(user.firstName)
        lastName.setText(user.lastName)
        email.setText(user.email)
        phone.setText(user.phone)
        Picasso.get().load(BuildConfig.IMG_URL + imageUrl).into(image)
        changePassword.setOnClickListener { start(ChangePasswordActivity::class.java) }
    }
}