package com.boxes.driver.ui.orderlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.app.boxes.data.remote.Resource
import com.boxes.driver.base.BaseViewModel
import com.boxes.driver.data.model.Order
import com.boxes.driver.data.repository.OrderListRepository
import com.boxes.driver.data.request.DateFilter
import kotlinx.coroutines.launch

class OrderListViewModel(private val repository: OrderListRepository): BaseViewModel() {

    val orders = MutableLiveData<Resource<List<Order>>>()
    val history = MutableLiveData<Resource<List<Order>>>()

    fun getOrders() {
        viewModelScope.launch {
            orders.value = Resource.Loading()
            orders.value = repository.getOrders()
        }
    }

    fun getHistory(date: String = "") {
        viewModelScope.launch {
            history.value = Resource.Loading()
            history.value = repository.getHistory(DateFilter(date))
        }
    }

}