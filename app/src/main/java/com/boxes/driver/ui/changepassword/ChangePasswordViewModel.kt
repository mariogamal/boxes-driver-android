package com.boxes.driver.ui.changepassword

import androidx.lifecycle.liveData
import com.app.boxes.data.remote.Resource
import com.boxes.driver.base.BaseViewModel
import com.boxes.driver.data.repository.ChangePasswordRepository
import com.boxes.driver.data.request.ChangePassword

class ChangePasswordViewModel(private val repository: ChangePasswordRepository) : BaseViewModel() {

    fun changePassword(request: ChangePassword) = liveData {
        emit(Resource.Loading())
        emit(repository.changePassword(request))
    }
}