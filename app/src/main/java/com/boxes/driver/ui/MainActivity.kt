package com.boxes.driver.ui

import android.os.Bundle
import com.app.boxes.base.BaseFragment
import com.boxes.driver.R
import com.boxes.driver.base.BaseActivity
import com.boxes.driver.ui.orderlist.OrderListFragment
import com.boxes.driver.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> openFragment(OrderListFragment())
                R.id.profile -> openFragment(ProfileFragment())
            }
            true
        }
        bottomNavigation.selectedItemId = R.id.home
    }

    private fun openFragment(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragmentHolder, fragment).commit()
    }

}