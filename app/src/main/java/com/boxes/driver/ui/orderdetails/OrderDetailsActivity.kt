package com.boxes.driver.ui.orderdetails

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.boxes.driver.R
import com.boxes.driver.base.BaseActivity
import com.boxes.driver.data.model.Order
import com.boxes.driver.ui.orderlist.OrdersAdapter.Companion.ID
import kotlinx.android.synthetic.main.activity_order_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderDetailsActivity : BaseActivity() {

    private lateinit var id: String
    private val viewModel: OrderDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)
        back.setOnClickListener { finish() }
        intent.getStringExtra(ID)?.let {
            id = it
            observe(viewModel.orderDetails(it)) { bindView(it) }
        }
        markDelivered.setOnClickListener { observe(viewModel.markDelivered(id)) { finish() } }
    }

    @SuppressLint("SetTextI18n")
    private fun bindView(order: Order) {
        name.text = order.name
        phone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${order.phone}")
            startActivity(intent)
        }
        area.text = "${order.city}, ${order.area}, Block ${order.block}"
        street.text = "Street ${order.street}, Building ${order.building}, Floor ${order.floor}, Flat ${order.apartment},"
        mealsRecycler.adapter = MealsAdapter(order.meals)
    }

}