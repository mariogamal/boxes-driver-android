package com.boxes.driver.di

import com.boxes.driver.data.repository.LoginRepository
import com.boxes.driver.data.repository.OrderDetailsRepository
import com.boxes.driver.ui.changepassword.ChangePasswordViewModel
import com.boxes.driver.ui.login.LoginViewModel
import com.boxes.driver.ui.orderdetails.OrderDetailsViewModel
import com.boxes.driver.ui.orderlist.OrderListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { OrderListViewModel(get()) }
    viewModel { OrderDetailsViewModel(get()) }
    viewModel { ChangePasswordViewModel(get()) }
}