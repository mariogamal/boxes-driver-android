package com.boxes.driver.di

import android.content.Context
import com.boxes.driver.data.remote.RequestInterceptor
import com.boxes.driver.BuildConfig
import com.boxes.driver.data.local.Preferences
import com.boxes.driver.data.remote.BoxesAPI
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val prefModule = module {
    single { Preferences(androidContext()) }
}

val networkModule = module {
    single { RequestInterceptor(get()) }
    single { provideOkHttpClient(get(), androidContext()) }
    single { provideBoxesApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(requestInterceptor: RequestInterceptor, context: Context): OkHttpClient {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient()
        .newBuilder()
        .addInterceptor(requestInterceptor)
        .addInterceptor(loggingInterceptor)
        .addInterceptor(ChuckInterceptor(context))
        .build()
}

fun provideBoxesApi(retrofit: Retrofit): BoxesAPI = retrofit.create(BoxesAPI::class.java)