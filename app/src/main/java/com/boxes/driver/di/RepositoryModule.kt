package com.boxes.driver.di

import com.boxes.driver.data.repository.ChangePasswordRepository
import com.boxes.driver.data.repository.LoginRepository
import com.boxes.driver.data.repository.OrderDetailsRepository
import com.boxes.driver.data.repository.OrderListRepository
import org.koin.dsl.module

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
val repositoryModule = module {
    factory { LoginRepository(get()) }
    factory { OrderListRepository(get()) }
    factory { OrderDetailsRepository(get()) }
    factory { ChangePasswordRepository(get()) }
}