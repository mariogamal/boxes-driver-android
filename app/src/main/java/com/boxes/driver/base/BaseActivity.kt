package com.boxes.driver.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.app.boxes.data.remote.Resource
import com.boxes.driver.data.local.Preferences
import com.boxes.driver.util.ProgressDialog
import com.boxes.driver.util.snack
import org.koin.android.ext.android.inject

open class BaseActivity : LocalizationActivity() {

    val preferences: Preferences by inject()
    var rootView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootView = findViewById(android.R.id.content)
    }

    fun start(activity: Class<out BaseActivity>, extras: Bundle? = null) {
        val intent = Intent(this, activity)
        extras?.let { intent.putExtras(it) }
        startActivity(intent)
    }

    fun <T> observe(liveData: LiveData<Resource<T>>, result: (data: T) -> Unit) {
        liveData.removeObservers(this)
        liveData.observe(this, {
            when (it) {
                is Resource.Loading -> ProgressDialog.getInstance().show(this)
                is Resource.Success -> {
                    ProgressDialog.getInstance().dismiss()
                    result(it.data ?: Any() as T)
                }
                is Resource.Error -> {
                    ProgressDialog.getInstance().dismiss()
                    it.message?.let { e -> if (e != "") rootView?.snack(e) }
                }
            }
        })
    }

    fun showMessage(message: String) {
        rootView?.snack(message)
    }

}