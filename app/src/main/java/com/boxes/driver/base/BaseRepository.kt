package com.boxes.driver.base

import com.app.boxes.data.remote.Resource
import com.boxes.driver.data.model.Response
import com.boxes.driver.data.remote.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

open class BaseRepository {

    suspend fun <T> handleResponse(apiMethod: suspend () -> Response<T>): Resource<T> =
            withContext(Dispatchers.IO) {
                try {
                    ResponseHandler.handleSuccess(apiMethod())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    ResponseHandler.handleException(ex)
                }
            }
}