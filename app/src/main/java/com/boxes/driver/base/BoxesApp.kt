package com.app.boxes.base

import com.akexorcist.localizationactivity.ui.LocalizationApplication
import com.boxes.driver.di.repositoryModule
import com.boxes.driver.di.viewModelModule
import com.boxes.driver.di.networkModule
import com.boxes.driver.di.prefModule
import com.onesignal.OneSignal
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class BoxesApp : LocalizationApplication() {

    override fun getDefaultLanguage(): Locale = Locale.ENGLISH

    override fun onCreate() {
        super.onCreate()
        OneSignal.initWithContext(this)
        OneSignal.setAppId("ce14b06b-3f62-4e9a-80a1-56e02fe5421b")
        startKoin {
            androidContext(this@BoxesApp)
            modules(listOf(prefModule, networkModule, repositoryModule, viewModelModule))
        }
    }
}